if SelfTarget then return end
SelfTarget = {}

function SelfTarget_Initialize()
	SelfTarget.DEBUG  = false
	SelfTarget.ACTIVE = true
	if( LibTimer == nil ) then
		print("SelfTarget ERROR: The dependency LibTimer could not be found, make sure you installed the Addon LibTimer in your Interface/Addons/ Directory")
		return false
	end
	if( LibSlash == nil ) then
		print("SelfTarget ERROR: The dependency LibSlash could not be found, make sure you installed the Addon LibSlash in your Interface/Addons/ Directory")
		return false
	end	
	-- Register Slash Commands, thanks again to Aiiane for LibSlash
    LibSlash.RegisterSlashCmd("selftarget", function(input) SelfTarget.SlashHandler(input) end)
	-- Adding 1second repeating Timer
	LibTimer.AddTask("SelfTarget",1,function() SelfTarget.Check() end, true)
	print("WAR-Addon 'SelfTarget` by sOLARiZ loaded, happy healing!")
end

function SelfTarget_Shutdown()
	LibTimer.DelTask("SelfTarget")
end

function SelfTarget.Check()
	if (SelfTarget.ACTIVE == true) then
		SelfTarget.wdebug("Check")
		local MyFriendlyTarget = TargetInfo:UnitEntityId("selffriendlytarget")
		if( MyFriendlyTarget == nil or MyFriendlyTarget == 0 ) then
			-- No Target, Target self
			SelfTarget.wdebug("Target Self")
			TargetPlayer(towstring(GameData.Player.name))
		end
	end
end

function SelfTarget.SlashHandler(input)
	SelfTarget.wdebug("SlashHandler")
	if(SelfTarget.ACTIVE == true) then
		SelfTarget.wdebug("Deactivating")
		SelfTarget.ACTIVE = false
		LibTimer.PauseTask("SelfTarget",true)
		print("SelfTarget Disabled")
	else
		SelfTarget.wdebug("Activating")
		SelfTarget.ACTIVE = true
		LibTimer.PauseTask("SelfTarget",false)
		print("SelfTarget Enabled")
	end
end

function print(arg)
      if(arg)
      then
	      ChatWindow.Print(towstring(arg))
      end
end

function SelfTarget.wdebug(msg)
	-- check if debug is enabled
	if( SelfTarget.DEBUG ~= true and msg ~= nil) then return end
	-- go ahead give Dbg message
	d(L"SelfTarget> "..towstring(msg))
	-- Checking Tasks
end

<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="SelfTarget" version="release_rev2" date="2008-09-13T18:52:25Z" >

		<Author name="solariz" email="ad@solariz.de" />
		<Description text="If you deselect your Friendly Target this Addon target youself. This is very helpfull as Healer to not lose the focus because some DOTs are friendly target depending." />
		
    <Dependencies>
        <Dependency name="EA_ChatWindow" />
		<Dependency name="LibTimer" />
		<Dependency name="LibSlash" />
    </Dependencies>
        
		<Files>
			<File name="SelfTarget.lua" />
		</Files>
		
		<OnInitialize>
			<CallFunction name="SelfTarget_Initialize" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown>
		  <CallFunction name="SelfTarget_Shutdown" />
		</OnShutdown>
		
	</UiMod>
</ModuleFile>
